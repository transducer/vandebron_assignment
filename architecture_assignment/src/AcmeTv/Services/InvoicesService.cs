using AcmeTv.Data.Contracts;
using AcmeTv.Data.Model;
using AcmeTv.Services.Contracts;

using System;
using System.Collections.ObjectModel;

namespace AcmeTv.Services 
{
    public class InvoiceService : IInvoiceService 
    {
        private readonly IRepository repository;

        public InvoiceService(IRepository repository) 
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));
            this.repository = repository;
        }

        public Collection<Invoice> AggregateInvoices(int customerId) 
        {
            return new Collection<Invoice> { new Invoice { PeriodDescription = "HELLO WORLD" } };
        }
    }
}