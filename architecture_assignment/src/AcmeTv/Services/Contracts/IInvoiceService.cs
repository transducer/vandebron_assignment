using AcmeTv.Data.Model;

using System.Collections.ObjectModel;

namespace AcmeTv.Services.Contracts {
    public interface IInvoiceService {
        Collection<Invoice> AggregateInvoices(int customerId);
    }
}