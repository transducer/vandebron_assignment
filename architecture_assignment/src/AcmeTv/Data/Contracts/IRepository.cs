using AcmeTv.Data.Model;

using System.Collections.Generic;

namespace AcmeTv.Data.Contracts 
{
    public interface IRepository 
    {
        IEnumerable<Invoice> GetInvoices(int customerId);
    }
}