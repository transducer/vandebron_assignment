using System;

namespace AcmeTv.Data.Model 
{
    public class Invoice 
    {
           public int CustomerId { get; set; }
           public string AddressId { get; set; }
           public string InvoiceId { get; set; }
           public string InvoiceTypeLocalized { get; set; }
           public DateTime InvoiceDate { get; set; }
           public DateTime PaymentDueDate { get; set; }
           public int InvoiceNumber { get; set; }
           public DateTime StartDate { get; set; }
           public DateTime EndDate { get; set; }
           public string PeriodDescription { get; set; }
           public decimal Amount { get; set; }
           public decimal VatAmount { get; set; }
           public decimal TotalAmount { get; set; }
    }
}