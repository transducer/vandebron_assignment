using AcmeTv.Services.Contracts;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AcmeTv.Controllers
{
    [Route("sysapi/" + "v1.0" + "/[controller]")]
    public class InvoicesController : Controller
    {
        private readonly IInvoiceService invoiceService;
        
        public InvoicesController(IInvoiceService invoiceService) 
        {
            if (invoiceService == null) throw new ArgumentNullException(nameof(invoiceService));
            this.invoiceService = invoiceService;
        }

        // GET /sysapi/v1.0/invoices/?customerId=1&addressId=8212BJ154
        [HttpGet("{customerId}/{addressId}")]
        public string Get(int customerId, string addressId)
        {
            return invoiceService.AggregateInvoices(customerId).First().PeriodDescription;
        }
 
        // GET /sysapi/v1.0/invoices/?customerId=1&month=3/ 
        [HttpGet("{customerId}/{month}")]
        public string Get(int customerId, int month)
        {
            throw new NotImplementedException();
        }

        // GET /sysapi/v1.0/invoices/?customerId=1&filter=shop&month=3
        [HttpGet("{customerId}/{filter}/{month}")]
        public IEnumerable<int> Get(int customerId, string filter, int month)
        {
            throw new NotImplementedException();
        }
       
        // GET /sysapi/v1.0/invoices/?customerId=14
        [HttpGet("{customerId}")]
        public IEnumerable<int> Get(int customerId)
        {
            throw new NotImplementedException();
        }

        // POST /sysapi/v1.0/invoices
        [HttpPost]
        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }
   }
}