# Vandebron assignments

This repository contains the three Vandebron assignments.

## Description

#### Architecture assignment

I tried to adhere to the requirements, but because I do not have the time to create a full project I have only setup a basic API structure. It makes use of the repository pattern and dependency injection.

Here is an ER diagram from where I can imagine the data as shown in the JSON responses comes from and how it can be logically and transparently structured.

![ER Diagram](https://gitlab.com/erwin_rooijakkers/vandebron_assignment/raw/master/er_diagram.png)

#### Salesforce Visualforce assignment

A Celsius to Fahrenheit converter created on the Salesforce platform.

#### FooBar Scala Assignment

This solves a FizzBuzz exercise with a Scala sauce.

Assignment:
>Return “foo”, “bar” or “foobar”.<br>
For a given natural number greater than zero return<br>
1. “foo” if the number is dividable by 3<br>
2. “bar” if the number is dividable by 7<br>
3. “foobar” if the number is dividable by 21<br>
4. the number itself if none of the above apply.

The function that calculates the FooBar must take as input an Option[FooBar]. FooBar is a case class that contains the int value.

The implementation is in one file which contains a very simple hand rolled unit-testing framework, the solution and an example print of how the fucntion handles the first 50 natural numbers. It was built using test-driven development.

Other implementations:
* *FizzBuzz Enterprise Edition is a no-nonsense implementation of FizzBuzz made by serious businessmen for serious business purpose*<br>[FizzBuzzEnterprise](https://github.com/EnterpriseQualityCoding/FizzBuzzEnterpriseEdition)
* *(fizz) buzz around TensorFlow and machine learning*<br>[Machine learning FizzBuzz](https://cloud.google.com/blog/big-data/2016/05/the-fizz-buzz-around-tensorflow-and-machine-learning)

## Building and running the solutions

### Architecture assignment

The application is built in ASP.NET 5 and runs on Windows, Linux or Mac.

Install .NET Core for your platform of choice by following the instructions on [https://www.microsoft.com/net/core](https://www.microsoft.com/net/core).

Then execute the following two commands in the root of the AcmeTv project to build and run the application:

    dotnet restore
    dotnet run

The only resource that is implemented can be retrieved via GET http://localhost:5000/sysapi/v1.0/invoices/1/hello.

#### Salesforce Visualforce assignment

The code can be viewed at [SalesForce](https://login.salesforce.com/).

Username: erwin@vandebron.nl
Password: P@ssw0rd

* [TemperatureConversion page](https://c.eu6.visual.force.com/apex/TemperatureConversion)
* [Classes and test](https://eu6.salesforce.com/01p?retURL=%2Fui%2Fsetup%2FSetup%3Fsetupid%3DDevToolsIntegrate&setupid=ApexClasses)

#### FooBar Scala Assignment

Scala needs to be installed on your machine to be able to run the solution. Instructions for installations can be found on [the official site](http://www.scala-lang.org/download/install.html).

To show what the function results in for the first 50 natural numbers run the solution:

    scala foobar.scala

To run the test cases the following command can be run:

    scala foobar.scala test

