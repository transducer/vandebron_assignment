/**
  * Return “foo”, “bar” or “foobar”.
  * For a given natural number greater than zero return:
  * - “foo” if the number is dividable by 3
  * - “bar” if the number is dividable by 7
  * - “foobar” if the number is dividable by 21
  * - the number itself if none of the above apply.
  *
  * The function that calculates the FooBar must take as input an Option[FooBar]. FooBar is a case class that contains the int value.
  */

// logic to have a test runner without having to introduce a testing framework
def getFirstArgument: Option[String] = if (args.length > 0) Some(args(0)) else None

object commandLineIdentifiers {
  val test: String = "test"
  val run: String = "run"
}

def processArgument(arg: Option[String]): Unit = {
  arg match {
    case Some(a) if (a == commandLineIdentifiers.test) => test
    case Some(_) => println("Only valid command line parameter is test. \nUsage: foobar test")
    case None => run
  }
}

val commandLineArgument = getFirstArgument
processArgument(commandLineArgument)

// foobar implementation code
case class FooBar(n: Int)
def calculateFoobar(fooBar: Option[FooBar]): String =
  fooBar match {
    case Some(FooBar(n)) if (n % 21 == 0) => "foobar"
    case Some(FooBar(n)) if (n % 3 == 0) => "foo"
    case Some(FooBar(n)) if (n % 7 == 0) => "bar"
    case Some(FooBar(n)) if (n > 0) => n.toString
    case _ => "" // Not defined in the spec. Returning empty string seems reasonable.
  }

// test cases
def test: Unit = {
  def assertSuccessful(): Unit = println("SUCCESSFUL")
  def assertTrue(expressionToAssert: Boolean): Unit = if (expressionToAssert) println("SUCCESSFUL") else println("FAILED")

  println("The function that calculates FooBar...")
  println("should take Option[FooBar] as input")
  calculateFoobar(Some(FooBar(1))); assertSuccessful()

  println("and FooBar is a case class that should take an int")
  calculateFoobar(Some(FooBar(1))); assertSuccessful()

  println("should return \"foo\" when the number is divisible by 3 but not 7")
  assertTrue(calculateFoobar(Some(FooBar(3))) == "foo")
  assertTrue(calculateFoobar(Some(FooBar(6))) == "foo")
  assertTrue(calculateFoobar(Some(FooBar(12))) == "foo")

  println("should return \"bar\" when the number is divisible by 7 but not 3")
  assertTrue(calculateFoobar(Some(FooBar(7))) == "bar")
  assertTrue(calculateFoobar(Some(FooBar(28))) == "bar")

  println("should return \"foobar\" when the number is divisible by both 7 and 3")
  assertTrue(calculateFoobar(Some(FooBar(21))) == "foobar")
  assertTrue(calculateFoobar(Some(FooBar(84))) == "foobar")

  println("return the number itself if positive and not divisible by 3 or 7")
  assertTrue(calculateFoobar(Some(FooBar(1))) == "1")
  assertTrue(calculateFoobar(Some(FooBar(2))) == "2")
  assertTrue(calculateFoobar(Some(FooBar(4))) == "4")

  println("return the empty string when the number is negative")
  assertTrue(calculateFoobar(Some(FooBar(-2))) == "")

  println("return the empty string when None is provided")
  assertTrue(calculateFoobar(None) == "")
}

// running an example
def run: Unit = {
  println("Example running FooBar from 1 to 50...")
  val fooBars = (1 to 50) map (n => Some(FooBar(n)))
  val processedFooBars = fooBars map calculateFoobar

  processedFooBars foreach println
}
